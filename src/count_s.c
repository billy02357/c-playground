#include <stdio.h>

int main() {

        long spaces;
        int c;

        spaces = 0;

        while( (c = getchar()) != 'q')
                if(c == ' ')
                        ++spaces;
        
        printf("%d", spaces);
}
