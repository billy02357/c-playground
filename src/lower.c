#include <stdio.h>

int lowerit(int c) {
  int res = (c >= 'A' && c <= 'Z') ? (c + 'a' - 'A') : c;
  return res;
}

int main(void) {
  int out[5] = { 'A', 'B', 'C', 'D', 'E' };

  for( int i = 0; i < 5; ++i ) {
    printf("%c", lowerit(out[i]));
  }
  printf("\n");
  
  return 0;
}
