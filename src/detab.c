#include <stdio.h>

int main() {

        int c;
        
        while( (c = getchar()) != '\r')
                if( c == '\t') {
                        putchar(' ');
                        putchar(' ');
                        putchar(' ');
                }
                else if( c == ' ') {
                        putchar(' ');
                        putchar(' ');
                        putchar(' ');
                }
                else
                        putchar(c);
}
