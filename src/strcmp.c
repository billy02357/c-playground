int strcmp(char *s, char *t)
{
	int i = 0;

	for (; s[i] == t[i]; i++) {
		if (s[i] == '\0') {
			return 0;
		}
	}

	return s[i] - t[i];
}

int strcmpp(char *s, char *t)
{
	int i = 0;

	for (; *s == *t; s++, t++) {
		if (*s == '\0') {
			return 0;
		}
	}

	return *s - *t;
}
