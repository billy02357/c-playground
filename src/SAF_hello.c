#define SAF_PROGRAM_NAME "hello"
#define SAF_PLATFORM_SDL2

#include "saf.h"

void SAF_init(void) {
}

uint8_t SAF_loop() {
    SAF_clearScreen(SAF_COLOR_WHITE);

    SAF_drawText(4, 15, "HELLO!", SAF_frame() & 0x04 ? SAF_COLOR_RED : SAF_COLOR_BLACK, 2);

    printf("got here!\n");

    return 0;
}
