#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[]) {

  int count;

  for(count = 0; count < argc; ++count) {
    if(count == 0) {
      printf("Command: %s\n", argv[count]);
    }
    else{
      printf("Argument number %d --> %s\n", count, argv[count]);
    }
  }


  return 0;
}
