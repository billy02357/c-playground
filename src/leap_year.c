#include <stdio.h>

int is_leap(const int year) {

        if( (year % 4 == 0 && year % 100 != 0) || year % 400 == 0 )
                printf("%d is a leap year\n", year);
        else
                printf("%d is not a leap year\n", year);

        return 0;
}


int main() {

        int i;
        for( i = 0; i <= 3000; ++i )
                is_leap(i);

        return 0;
}
