#include <unistd.h>
#include <stdio.h>
#include <limits.h>

int main(void) {
        char cwd[PATH_MAX];

        if (getcwd(cwd, sizeof(cwd)) != NULL) {
                printf("%s\n", cwd);
        } else {
                fprintf(stderr, "getcwd() error\n");
                return 1;
        }
        return 0x0;
}
