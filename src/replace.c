#include <stdio.h>

int main() {

        int c;
        
        while( (c = getchar()) != '\r') {
                if( c == '\t') {
                        putchar(92);
                        putchar('t');
                }
                else if( c == '\b') {
                        putchar(92);
                        putchar('b');
                }
                else if( c == '\\') {
                        putchar(92);
                        putchar(92);
                }
                else if( c == ' ') {
                        putchar(92);
                        putchar(' ');
                }
                else
                        putchar(c);
        }
}
