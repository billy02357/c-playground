#include <stdio.h>

int main() {

        int c, tabs;
        
        tabs = 0;

        while( (c = getchar()) != 'q')
                if( c == '\t')
                        ++tabs;

        printf("%d", tabs);
}
