#include <stdio.h>

int main() {

  int intvar = 100;
  float floatvar = 331.79;
  double doublevar = 8.44e+11;
  char charvar = 'W';
  _Bool boolvar = 0;

  printf("intvar = %i\n", intvar);
  printf("floatvar = %f\n", floatvar);
  printf("doublevar = %e\n", doublevar);
  printf("doublevar = %g\n", doublevar);
  printf("charvar = %c\n", charvar );
  printf("boolvar = %i\n", boolvar);

return 0;
}
