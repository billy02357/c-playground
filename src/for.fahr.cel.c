#include <stdio.h>

#define BASE    0
#define MAX     300
#define STEP    20

int main(){
        int fahr;

        for(fahr = BASE; fahr <= MAX; fahr = fahr + STEP)
                printf("%3d %6.1f\n", fahr, (5.0/9.0)*(fahr-32));
}
